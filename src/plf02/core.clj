(ns plf02.core)

(defn función-associative?-1
  [x]
  (associative? x))

(defn función-associative?-2
  [x y]
  (associative? (conj x y)))

(defn función-associative?-3
  [x y]
  (associative? (list x y)))

(función-associative?-1 [23 22 12])
(función-associative?-2 {:k 23} {})
(función-associative?-3 #{:a :b :c} [23 23])

(defn función-boolean?-1
  [x]
  (boolean? x))

(defn función-boolean?-2
  [x y]
  (boolean? (or (boolean? x) (boolean? y))))

(defn función-boolean?-3
  [x]
  (boolean? (boolean? x)))

(función-boolean?-1 false)
(función-boolean?-2 true 23)
(función-boolean?-3 #{:a :b :c})

(defn función-char?-1
  [x]
  (char? x))

(defn función-char?-2
  [x y]
  (char? (str x y)))

(defn función-char?-3
  [x]
  (char? (char x)))

(función-char?-1 \e)
(función-char?-2 \2 23)
(función-char?-3 1)

(defn función-coll?-1
  [x]
  (coll? x))

(defn función-coll?-2
  [x y]
  (coll? (list x y)))

(defn función-coll?-3
  [x]
  ( coll? [(coll? x)]))

(función-coll?-1 {:k 23})
(función-coll?-2 \@ 23)
(función-coll?-3 1)

(defn función-decimal?-1
  [x]
  (decimal? x))

(defn función-decimal?-2
  [x y z]
  (decimal? (+ x y z)))

(defn función-decimal?-3
  [x y]
  (decimal? (- x y))) 

(función-decimal?-1 12)
(función-decimal?-2 0.05 0.0000005 23)
(función-decimal?-3 1 1)

(defn función-float?-1
  [x]
  (float? x))

(defn función-float?-2
  [x y]
  (float? (+ x y)))

(defn función-float?-3
  [x y]
  (float? (- x y)))

(función-float?-1 12)
(función-float?-2 2 3)
(función-float?-3 1.1 3.1)

(defn función-ident?-1
  [x]
  (ident? x))

(defn función-ident?-2
  [x y]
  (ident? (or x y)))

(defn función-ident?-3
  [x y]
  (ident? (list x y)))

(función-ident?-1 'miau)
(función-ident?-2 :a 22)
(función-ident?-3 :k :w)

(defn función-indexed?-1
  [x]
  (indexed? x))

(defn función-indexed?-2
  [x y]
  (indexed? (conj x y)))

(defn función-indexed?-3
  [x y z]
  (indexed? (conj x y z)))

(función-indexed?-1 [:k 2 23 \a])
(función-indexed?-2 '(23 12 56 55) {:k 23})
(función-indexed?-3 [:k 45] [23 22 12] 23)

(defn función-int?-1
  [x]
  (int? x))

(defn función-int?-2
  [x y]
  (int? (+ x y)))

(defn función-int?-3
  [x y]
  (int? (- x y)))

(función-int?-1 12)
(función-int?-2 2 3)
(función-int?-3 1 3)

(defn función-integer?-1
  [x]
  (integer? x))

(defn función-integer?-2
  [x y]
  (integer? (- x y)))

(defn función-integer?-3
  [x y z]
  (integer? (* x y z)))

(función-integer?-1 12)
(función-integer?-2 2 3)
(función-integer?-3 1 3 :a)

(defn función-keyword?-1
  [x]
  (keyword? x))

(defn función-keyword?-2
  [x y]
  (keyword? (str x y)))

(defn función-keyword?-3
  [x]
  (keyword? (keyword x)))

(función-keyword?-1 :a)
(función-keyword?-2 \a 23)
(función-keyword?-3 1)

(defn función-list?-1
  [x]
  (list? x))

(defn función-list?-2
  [x y]
  (list? (vector x y)))

(defn función-list?-3
  [x y]
  (list? (list x y)))

(función-list?-1 [23 \a])
(función-list?-2 \a 3)
(función-list?-3 '(1) '(3))

(defn función-map-entry?-1
  [x]
  (map-entry? x))

(defn función-map-entry?-2
  [x y]
  (map-entry? (conj x y)))

(defn función-map-entry?-3
  [x y]
  (map-entry? (list x y)))

(función-map-entry?-1 [23 \a])
(función-map-entry?-2 :a {:a 23})
(función-map-entry?-3 1 3)

(defn función-map?-1
  [x]
  (map? x))

(defn función-map?-2
  [x y]
  (map? (map x y)))

(defn función-map?-3
  [x y]
  (map? (vector x y)))

(función-map?-1 [23 \a])
(función-map?-2 {:1 \a} {:2 \a})
(función-map?-3 1 3)

(defn función-nat-int?-1
  [x]
  (nat-int? x))

(defn función-nat-int?-2
  [x y]
  (nat-int? (+ x y)))

(defn función-nat-int?-3
  [x y]
  (nat-int? (* x y)))

(función-nat-int?-1 -23)
(función-nat-int?-2 1 3)
(función-nat-int?-3 -1 -3)

(defn función-number?-1
  [x]
  (number? x))

(defn función-number?-2
  [x y]
  (number? (+ x y)))

(defn función-number?-3
  [x y]
  (number? (- x y)))

(función-number?-1 -23)
(función-number?-2 [3] 2)
(función-number?-3 3 4)

(defn función-pos-int?-1
  [x]
  (pos-int? x))

(defn función-pos-int?-2
  [x y]
  (pos-int? (+ x y)))

(defn función-pos-int?-3
  [x y z]
  (pos-int? (* x y z)))

(función-pos-int?-1 -23)
(función-pos-int?-2 1 3)
(función-pos-int?-3 1 5 7)

(defn función-ratio?-1
  [x]
  (ratio? x))

(defn función-ratio?-2
  [x y]
  (ratio? (+ x y)))

(defn función-ratio?-3
  [x y]
  (ratio? (- x y)))

(función-ratio?-1 -23)
(función-ratio?-2 1/6 23)
(función-ratio?-3 1/2 3)

(defn función-rational?-1
  [x]
  (rational? x))

(defn función-rational?-2
  [x y]
  (rational? (+ x y)))

(defn función-rational?-3
  [x y]
  (rational? (- x y)))

(función-rational?-1 -23)
(función-rational?-2 1 1/6)
(función-rational?-3 1 3)

(defn función-seq?-1
  [x]
  (seq? x))

(defn función-seq?-2
  [x y]
  (seq? (vector x y)))

(defn función-seq?-3
  [x y]
  (seq? (list x y)))

(función-seq?-1 '(22 23 24))
(función-seq?-2 1 2)
(función-seq?-3 1 3)

(defn función-seqable?-1
  [x]
  (seqable? x))

(defn función-seqable?-2
  [x y]
  (seqable? (vector x y)))

(defn función-seqable?-3
  [x y]
  (seqable? (list x y)))

(función-seqable?-1 '(22 23 24))
(función-seqable?-2 1 2)
(función-seqable?-3 1 3)

(defn función-sequential?-1
  [x]
  (sequential? x))

(defn función-sequential?-2
  [x y]
  (sequential? (vector x y)))

(defn función-sequential?-3
  [x y]
  (sequential? (list x y)))

(función-sequential?-1 '(22 23 24))
(función-sequential?-2 :k 2)
(función-sequential?-3 1 3)

(defn función-set?-1
  [x]
  (set? x))

(defn función-set?-2
  [x y]
  (set? (vector x y)))

(defn función-set?-3
  [x y]
  (set? (list x y)))

(función-set?-1 (hash-set 22 23 24))
(función-set?-2 (hash-set 22 23 24) (hash-set 22 23 24))
(función-set?-3 1 3)


(defn función-some?-1
  [x]
  (some? x))

(defn función-some?-2
  [x y]
  (some? (vector x y)))

(defn función-some?-3
  [x y z]
  (some? (list x y z)))

(función-some?-1 (hash-set 22 23 24))
(función-some?-2 (hash-set 22 23 24) (hash-set 22 23 24))
(función-some?-3 1 3 5)

(defn función-string?-1
  [x]
  (string? x))

(defn función-string?-2
  [x y]
  (string? (vector x y)))

(defn función-string?-3
  [x y]
  (string? (concat x y)))

(función-string?-1 "(hash-set 22 23 24)")
(función-string?-2 "Hola" "Adios")
(función-string?-3 "a" 3)

(defn función-symbol?-1
  [x]
  (symbol? x))

(defn función-symbol?-2
  [x y]
  (symbol? (concat x y)))

(defn función-symbol?-3
  [x y]
  (symbol? (str x y)))

(función-symbol?-1 'a)
(función-symbol?-2 "Hola" "Adios")
(función-symbol?-3 'a 23)

(defn función-vector?-1
  [x]
  (vector? x))

(defn función-vector?-2
  [x y]
  (vector? (list x y)))

(defn función-vector?-3
  [x y]
  (vector? (vector (list x y))))

(función-vector?-1 ['a])
(función-vector?-2 {:h "Hola"} {:a "Adios"})
(función-vector?-3 1 3)


(defn función-drop-1
  [n x]
  (drop n x))

(defn función-drop-2
  [x]
  (drop x))

(defn función-drop-3
  [n x y]
  (drop n (range x y)))

(función-drop-1 0 [23 21 12])
(función-drop-2 23)
(función-drop-3 1 2 5)

(defn función-drop-last-1
  [n x]
  (drop-last n x))

(defn función-drop-last-2
  [x]
  (drop-last x))

(defn función-drop-last-3
  [n x y]
  (drop-last n (range x y)))

(función-drop-last-1 1 [23 33 12])
(función-drop-last-2 {:k 1 :k2 2 :k3 5})
(función-drop-last-3 1 20 30)

(defn función-drop-while-1
  [x]
  (drop-while neg? x))

(defn función-drop-while-2
  [x]
  (drop-while float? x))

(defn función-drop-while-3
  [x]
  (drop-while pos-int? x))

(función-drop-while-1 [-23 33 12])
(función-drop-while-2 [1 5 4.6 32 5.1])
(función-drop-while-3 [23 20 30 -12])

(defn función-every?-1
  [x]
  (every? neg? x))

(defn función-every?-2
  [x]
  (every? float? x))

(defn función-every?-3
  [x]
  (every? pos-int? x))

(función-every?-1 [-23 -33 -12])
(función-every?-2 [1.5 4.6 32.22 5])
(función-every?-3 [23 20 30 12])

(defn función-filterv-1
  [x]
  (filterv neg? x))

(defn función-filterv-2
  [x]
  (filterv float? x))

(defn función-filterv-3
  [x]
  (filterv pos-int? x))

(función-filterv-1 [-23 33 -12])
(función-filterv-2 [1.5 4.6 32.22 5])
(función-filterv-3 [23 -20 30 12 23 -21 13])

(defn función-group-by-1
  [x]
  (group-by neg? x))

(defn función-group-by-2
  [x]
  (group-by float? x))

(defn función-group-by-3
  [x]
  (group-by pos-int? x))

(función-group-by-1 [-23 33 -12])
(función-group-by-2 [1.5 4.6 32.22 5])
(función-group-by-3 [23 -20 30 12 23 -21 13])

(defn función-iterate-1
  [x]
  (iterate inc x))

(defn función-iterate-2
  [x]
  (iterate dec x))

(defn función-iterate-3
  [x]
  (iterate partial x))

(función-iterate-1 5)
(función-iterate-2 6)
(función-iterate-3 2)

(defn función-keep-1
  [x]
  (keep even? x))

(defn función-keep-2
  [x]
  (keep pos? x))

(defn función-keep-3
  [x]
  (keep odd? x))

(función-keep-1 [5 15])
(función-keep-2 [6 10])
(función-keep-3 [2 4 5])

(defn función-keep-indexed-1
  [x]
  (keep-indexed #(if (odd? %1) %2) x))

(defn función-keep-indexed-2
  [x]
  (keep-indexed #(if (even? %1) %2) x))

(defn función-keep-indexed-3
  [x]
  (keep-indexed #(if (pos? %1) %2) x))

(función-keep-indexed-1 [:a :b :c :d :e])
(función-keep-indexed-2 [:a :b :c :d :e])
(función-keep-indexed-3 [:a :b :c :d :e])

(defn función-map-indexed-1
  [x]
  (map-indexed #(if (odd? %1) %2) x))

(defn función-map-indexed-2
  [x]
  (map-indexed #(if (even? %1) %2) x))

(defn función-map-indexed-3
  [x]
  (map-indexed #(if (pos? %1) %2) x))

(función-map-indexed-1 [:a :b :c :d :e])
(función-map-indexed-2 [:a :b :c :d :e])
(función-map-indexed-3 [:a :b :c :d :e])

(defn función-mapcat-1
  [x]
  (mapcat reverse x))

(defn función-mapcat-2
  [x]
  (mapcat concat x))

(defn función-mapcat-3
  [x y]
  (mapcat list x y))

(función-mapcat-1 [[:a :b] [:c :d :e]])
(función-mapcat-2 [[:a :b] [:c :d :e]])
(función-mapcat-3 [:a :b :c :d :e] [1 2 3 4 5])

(defn función-mapv-1
  [x]
  (mapv reverse x))

(defn función-mapv-2
  [x]
  (mapv concat x))

(defn función-mapv-3
  [x y]
  (mapv vector x y))

(función-mapv-1 [[:a :b] [:c :d :e]])
(función-mapv-2 [[:a :b] [:c :d :e]])
(función-mapv-3 [2 3 4 5 6] [1 2 3 4 5 6])

(defn función-merge-with-1
  [x y]
  (merge-with into x y))

(defn función-merge-with-2
  [x y]
  (merge-with concat x y))

(defn función-merge-with-3
  [x y]
  (merge-with conj x y))

(función-merge-with-1 {:Nombre ["Daniel"] :Apellido ["Valdez"]} {:Nombre ["Omar"] :Apellido ["Chavez"]})
(función-merge-with-2 {:a ["Hi"] :b ["Good"]} {:a ["Hello"] :b ["Morning"]})
(función-merge-with-3 {:numeros [1 2 3 4] :letras ["a" "b" "c"]} {:numeros [5 6 7] :letras ["x" "y" "z"]})

(defn función-not-any?-1
  [x]
  (not-any? odd? x))

(defn función-not-any?-2
  [x]
  (not-any? pos? x))

(defn función-not-any?-3
  [x]
  (not-any? boolean? x))

(función-not-any?-1 [1 3 4 5 6 7])
(función-not-any?-2 [-2 -3 -5 -6])
(función-not-any?-3 [1 3 true 4 5])

(defn función-not-every?-1
  [x]
  (not-every? odd? x))

(defn función-not-every?-2
  [x]
  (not-every? pos? x))

(defn función-not-every?-3
  [x]
  (not-every? boolean? x))

(función-not-every?-1 [1 3 4 5 6 7])
(función-not-every?-2 [-2 -3 -5 -6])
(función-not-every?-3 [1 3 true 4 5])

(defn función-partition-by-1
  [x]
  (partition-by odd? x))

(defn función-partition-by-2
  [x]
  (partition-by pos? x))

(defn función-partition-by-3
  [x]
  (partition-by boolean? x))

(función-partition-by-1 [1 3 4 5 6 7])
(función-partition-by-2 [-2 -3 -5 -6])
(función-partition-by-3 [1 3 true 4 5])

(defn función-reduce-kv-1
  [x]
  (reduce-kv (fn [m k v]
               (assoc m k (shuffle v))) {} x))

(defn función-reduce-kv-2
  [x]
  (reduce-kv (fn [s k v]
               (if (#{:leche :pan :huevos} k)
                 (+ s v) s)) 0 x))

(defn función-reduce-kv-3
  [x]
  (reduce-kv (fn [s k v]
               (if (even? k) (+ s v) s)) 0 x))

(función-reduce-kv-1 {:a [1 2 3], :b [5 4 6], :c [20 21 100]})
(función-reduce-kv-2 {:leche 70 :pan 34 :huevos 70})
(función-reduce-kv-3 [0 3 4 2 1 3 7 5 8 9])

(defn función-remove-1
  [x]
  (remove odd? x))

(defn función-remove-2
  [x]
  (remove pos? x))

(defn función-remove-3
  [x]
  (remove boolean? x))

(función-remove-1 [1 3 4 5 6 7])
(función-remove-2 [-2 -3 -5 6])
(función-remove-3 [1 3 true 4 5])

(defn función-reverse-1
  [x]
  (reverse x))

(defn función-reverse-2
  [x y]
  (reverse (conj x y)))

(defn función-reverse-3
  [x]
  (reverse (vector x)))

(función-reverse-1 [1 3 4 5 6 7])
(función-reverse-2 [-2 -3 -5 6] [1 4 67 6])
(función-reverse-3 [2 4 6 8 10 11 12])

(defn función-some-1
  [x]
  (some odd? x))

(defn función-some-2
  [x]
  (some pos? x))

(defn función-some-3
  [x]
  (some boolean? x))

(función-some-1 [1 3 4 5 6 7])
(función-some-2 [-2 -3 -5 6])
(función-some-3 [1 3 true 4 5])

(defn función-sort-by-1
  [x]
  (sort-by count x))

(defn función-sort-by-2
  [x]
  (sort-by #{:numerodecontrol} x))

(defn función-sort-by-3
  [x]
  (sort-by val > x))

(función-sort-by-1 ["palabra" "numero" "dos" "uno"])
(función-sort-by-2 [{:numerodecontrol 1716005} {:numerodecontrol 1716008} {:numerodecontrol 1316005} {:numerodecontrol 1516005}])
(función-sort-by-3 {:k1 23 :k2 14 :k3 34})

(defn función-split-with-1
  [x]
  (split-with odd? x))

(defn función-split-with-2
  [x]
  (split-with pos? x))

(defn función-split-with-3
  [x]
  (split-with boolean? x))

(función-split-with-1 [1 3 4 5 6 7])
(función-split-with-2 [-2 -3 -5 6])
(función-split-with-3 [1 3 true 4 5])


(defn función-take-1
  [x]
  (take 3 x))

(defn función-take-2
  [x y]
  (take 2 (conj x y)))

(defn función-take-3
  [x y]
  (take 2 (list x y)))

(función-take-1 [1 3 4 5 6 7])
(función-take-2 [2 5 6 89] [10 12 11 13])
(función-take-3 [2 5 6 89] [10 12 11 13])

(defn función-take-last-1
  [x]
  (take-last 3 x))

(defn función-take-last-2
  [x y]
  (take-last 2 (conj x y)))

(defn función-take-last-3
  [x]
  (take-last 0 x))

(función-take-last-1 [1 3 4 5 6 7])
(función-take-last-2 [2 5 7 9] [10 11 13 15])
(función-take-last-3 [-2 -4 5 23])

(defn función-take-nth-1
  [x]
  (take-nth 3 x))

(defn función-take-nth-2
  [x y]
  (take-nth 2 (conj x y)))

(defn función-take-nth-3
  [x]
  (take-nth 0 x))

(función-take-nth-1 [1 3 4 5 6 7])
(función-take-nth-2 [2 3 5 8] [10 11 13 16 20])
(función-take-nth-3 [-2 -4 5 23])

(defn función-take-while-1
  [x]
  (take-while odd? x))

(defn función-take-while-2
  [x]
  (take-while even? x))

(defn función-take-while-3
  [x]
  (take-while boolean? x))

(función-take-while-1 [1 3 4 5 6 7])
(función-take-while-2 [2 10])
(función-take-while-3 [-2 -4 5 23])

(defn función-update-1
  [x]
  (update x 0 inc))

(defn función-update-2
  [x]
  (update x :a #(str "Hola" %)))

(defn función-update-3
  [x]
  (update x 1 #(str "Hola" %)))

(función-update-1 [1 3 4 5 6 7])
(función-update-2 {})
(función-update-3 [-2 -4 5 23])

(defn función-update-in-1
  [x]
  (update-in x [:años] inc))

(defn función-update-in-2
  [x]
  (update-in x [:a] #(str "Hola" %)))

(defn función-update-in-3
  [x]
  (update-in x [:a :b] dec))

(función-update-in-1 {:años 23 :nombre "Dan"})
(función-update-in-2 {:a 23 :b 34})
(función-update-in-3 {:a {:b 12 :c 13} :d 25})
